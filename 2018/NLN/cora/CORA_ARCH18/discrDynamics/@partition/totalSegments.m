function n = totalSegments(Obj)
% returns total number of segments in Partition
% AP 31.7.17

n = prod(Obj.nrOfSegments);