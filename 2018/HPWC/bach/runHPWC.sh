runlim="runlim -s 4096 -t 3600 -r 3600"
path=benchmarks
bach=src/bach

DC="04 05 06"
for i in ${DC};
do
	model_name=dist_controller_${i}
	log1=ARCH2018/${model_name}.bach

	echo "running bach on example dist_controller_${i}"
	k=20
	$runlim -o ARCH2018/${model_name}.runlim ${bach} -sp benchmarks/dist_controller/${model_name}.xml benchmarks/dist_controller/${model_name}.cfg ${k} > ${log1}
done

TTEL="5 7 9"
for i in ${TTEL};
do
	model_name=TTEthernet_Simplified_${i}
	log1=ARCH2018/${model_name}.bach

	echo "running bach on example TTEthernet_Simplified_${i}"
	k=20
	$runlim -o ARCH2018/${model_name}.runlim ${bach} -sp benchmarks/TTEthernet/${model_name}.xml benchmarks/TTEthernet/${model_name}.cfg ${k} > ${log1}
done