`validation_results.mat` contains all the falsifying inputs found by zlscheck using the Zélus version of the models.

`validation_results` is a struct containing one field per model, then for each model one field per property, then for each property two cell-arrays `ok` and `notok`. The `ok` array contains the inputs that have been validated by MATLAB and the `notok` one the other inputs.

When `_instN` is not specified in a property name (e.g. in `cc1` in `chasingcars`), the same inputs scenarios were used for instance 1 and instance 2.

There are scripts in `https://github.com/ismailbennani/zlscheck/tree/arch20/arch/arch20/shared/benchmarks` called `plot_*.m` that take an input `u` from this struct and plot the results.

The inputs in `validation_results.mat` can be found in csv format at https://github.com/ismailbennani/zlscheck_arch20_counterexamples. The `validation_results` struct can be built by running the script `validate_all.m` found in `bench_results/`. The S-TALIRO toolbox is required as it is used to compute robustnesses in MATLAB.
