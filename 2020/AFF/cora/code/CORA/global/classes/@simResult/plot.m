function han = plot(obj,varargin)
% plot - plots a 2-dimensional projection of the simulated trajectories
%
% Syntax:  
%    han = plot(obj)
%    han = plot(obj,dim,color)
%
% Inputs:
%    obj - simResult object
%    dim - dimensions that should be projected
%    color - color of the plotted trajectory ('r','b','--b', etc.)
%
% Outputs:
%    han - handle for the resulting graphic object
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: simResult, simulateRandom

% Author:       Niklas Kochdumper
% Written:      06-June-2020
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

    % default values for the optional input arguments
    dims = [1,2];
    color = 'k';
    format = {};

    % parse input arguments
    if nargin > 1 && ~isempty(varargin{1})
       dims = varargin{1}; 
    end
    if nargin > 2 && ~isempty(varargin{2})
       color = varargin{2};
    end
    if nargin > 3 
       format = varargin(3:end); 
    end

    % loop over all simulated trajectories
    hold on
    for i = 1:length(obj.x)
       han = plot(obj.x{i}(:,dims(1)),obj.x{i}(:,dims(2)),color,format{:});
    end

%------------- END OF CODE --------------