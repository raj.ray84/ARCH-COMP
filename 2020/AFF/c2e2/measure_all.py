import os
import string
import re
from subprocess import Popen, PIPE
import time

string_path_host = os.getcwd()

tools = ['c2e2']

example = {'c2e2': []}
example['c2e2'].append(['building', 'BLDC01'])
example['c2e2'].append(['gearbox', 'GRBX01'])
example['c2e2'].append(['gearbox', 'GRBX02'])
example['c2e2'].append(['heat3d_c2e2', 'HEAT01'])
example['c2e2'].append(['iss', 'ISSC01_ISS02'])
example['c2e2'].append(['rendezvous', 'SRNA01'])
example['c2e2'].append(['rendezvous', 'SRA01'])
example['c2e2'].append(['rendezvous', 'SRA04'])

breakout_examples = 0

result_file = open("./result/results.csv",'w+')
result_file.write('')

for t in tools:
    for i in range(0,len(example[t])):
        work_dir = "/ARCH2020/"+example[t][i][0]+"/"+example[t][i][1]+"/Develop/"
        executable = work_dir+"test" 
        string_cmd = ["sudo docker run -w "+work_dir+" c2e2 "+executable]
        breakout_examples = 0

        print('CALLING: ', string_cmd)

        p = Popen(string_cmd, stdout=PIPE, stderr=PIPE, stdin=PIPE,shell = True)
        start_time = time.time()
        output = p.communicate()
        end_time = time.time()
        execution_time = end_time - start_time
        output = str(output[0],'utf-8')

        # print(output)

        fstr = 'Execution Time: '
        r_idx = output.rfind(fstr)
        r_idx_time_start = int(r_idx)+len(fstr)
        r_idx_time_end = int(r_idx)+len(fstr)+2
        out_time = output[r_idx_time_start:r_idx_time_end]
        print("out_time_before_re: ", execution_time)

        fstr = 'System is '
        r_idx = output.rfind(fstr)
        r_idx_result_start = int(r_idx)+len(fstr)
        r_idx_result_end = output.find("\n",r_idx)
        out_result = output[r_idx_result_start:r_idx_result_end]
        print("result is: ",out_result)
        
        verified = 0
        if out_result == "safe":
            verified = 1
        elif out_result == "unsafe":
            verified = -1
        else:
            verified = 0
        result_file.write("c2e2,"+example[t][i][1]+",,"+str(verified)+","+str(execution_time)+"\n")

result_file.close()
