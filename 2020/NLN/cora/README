We published the repeatability package for the CORA toolbox in Code Ocean (see https://codeocean.com/) with the DOI 10.24433/CO.1267711.v1 and the name "ARCHrepeatabilityCORA2020_nonlinear". On the Code Ocean website all results can be conveniently reproduced with a single click and without the necessity to install anything.

This directory contains the files for the exported Code Ocean capsule, which enables the generation of the results using Docker. For this, follow the following steps:

1. MAC-Address: Substitute the MAC address in the file "measure_all.sh" with the MAC address of your local machine.

2. MATLAB License: For the docker container to run properly, one has to create a new license file for the container. For this log-in with your MATLAB account at https://www.mathworks.com/licensecenter/licenses/ . Click on your license, and then navigate to "Install and Activate" -> "Activate to Retrieve License File" (may differ depending on how your licensing is set up). Create a new license file with the following data:

	- MATLAB version: 2019a, 
	- MAC address: MAC address of your local machine
	- User: root 

Finally, download the generated file "licence.lic" and put it into the directory "NLN/cora/"

3. Run the code: make the script "measure_all" executable by typing "chmod +x measure_all.sh" to the terminal. Then execute the script by typing "./measure all".
