function res = example_linear_reach_ARCH20_iss_ISSC01_ISU02()
% example_linear_reach_ARCH20_iss_ISSC01_ISU02 - ISS example from ARCH20
%   using the decomposed reachability algorithm following [1]
%
%
% Syntax:  
%    example_linear_reach_ARCH20_iss_ISSC01_ISU02
%
% Inputs:
%    no
%
% Outputs:
%    res - boolean 
%
% Example: -
%
% References: 
%   [1] S. Bogomolov, M. Forets, G. Frehse, A. Podelski, C. Schlling
%       "Decomposing Reach Set Computations with Low-dimensional Sets
%            and High-Dimensional Matrices"
% 
% Author:       Mark Wetzlinger
% Written:      09-June-2019
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------


% load system matrices
load iss.mat A B C

% construct extended system matrices (inputs as additional states)
dim = length(A);
A_  = [A,B;zeros(size(B,2),dim + size(B,2))];
B_  = zeros(dim+size(B,2),1);
C_  = [C,zeros(size(C,1),size(B,2))];

% construct the linear system object
sys = linearSys('iss',A_,B_,[],C_);


% Options -----------------------------------------------------------------
R0 = [interval(-0.0001*ones(dim,1),0.0001*ones(dim,1));interval([0;0.8;0.9],[0.1;1;1])];
params.R0             = zonotope(R0);
options.taylorTerms   = 10;
options.zonotopeOrder = 10;

options.linAlg        = 'decomp';
options.verbose       = false; % for logging every n iterations
options.specification = {halfspace([0 0 -1],-1.7e-4);halfspace([0 0 1],-1.7e-4)};
options.partition     = [1, 135; 136, 270; 271, 273];

params.uTrans           = 0; % center of input set
params.U                = zonotope(0); % input for reachability analysis
options.originContained = 1;

params.tFinal   = 20; % final time
params.timeStep = 0.02;
% -------------------------------------------------------------------------


% reachability analysis ---------------------------------------------------
tic
[Rout, ~, res] = reach(sys, params, options);
tComp = toc;
disp(['computation time of reachable set: ',num2str(tComp)]);
if ~res
    disp('Safety violation in reachability analysis!');
end
% -------------------------------------------------------------------------



% plot results ------------------------------------------------------------
hold on
i = 1;
options.blocks = length(options.partition);
relsafetyDim = 3;
% plot time elapse
while i <= length(Rout)
    % get Uout 
    t1 = (i-1)*options.timeStep;
    try
        minVal = inf;
        maxVal = -inf;
        for b=1:options.blocks
            if isa(Rout{i}{b},'zonotope')
                Uout = interval(project(Rout{i}{b},relsafetyDim));
                if infimum(Uout) < minVal
                    minVal = infimum(Uout);
                end
                if supremum(Uout) > maxVal
                    maxVal = supremum(Uout);
                end
            end
        end
        i = i + 1;
    catch
        for b=1:options.blocks
            if isa(Rout{i-1}{b},'zonotope')
                Uout = interval(project(Rout{i-1}{b},relsafetyDim));
                if infimum(Uout) < minVal
                    minVal = infimum(Uout);
                end
                if supremum(Uout) > maxVal
                    maxVal = supremum(Uout);
                end
            end
        end
    end
    t2 = (i-1)*options.timeStep;
    % generate plot areas as interval hulls
    IH1 = interval([t1; minVal], [t2; maxVal]);

    plotFilled(IH1,[1 2],[.75 .75 .75],'EdgeColor','none');
end

% plot unsafe set
plot(options.specification{1},[2,3]);
plot(options.specification{2},[2,3]);

% formatting
box on
xlabel('Time');
ylabel('y_3');
title('ISSC01-ISU02, reachDecomp');
grid on
%--------------------------------------------------------------------------

%example completed
res = 1;

%------------- END OF CODE --------------

