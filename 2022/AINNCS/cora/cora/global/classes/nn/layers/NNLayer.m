classdef (Abstract) NNLayer < handle
    % NNLayer - abstract class for nn layers
    %
    % Syntax:
    %    obj = NNLayer(name)
    %
    % Inputs:
    %    name - name of the layer, defaults to type
    %
    % Outputs:
    %    obj - generated object
    %
    % Other m-files required: none
    % Subfunctions: none
    % MAT-files required: none
    %
    % See also: NeuralNetwork
    %
    % Author:       Tobias Ladner
    % Written:      28-March-2022
    % Last update:  ---
    % Last revision:---

    %------------- BEGIN CODE --------------

    properties (Abstract, Constant)
        type % implemented in subclasses
        is_refinable % for adaptive evaluation
    end
    properties
        name % could be helpful for debugging
        sensitivity = 1
    end

    methods
        function obj = NNLayer(name)
            if nargin < 1
                name = NNLinearLayer.type;
            end

            obj.name = name;
        end
    end

    methods (Abstract)
        [nin, nout] = getNumNeurons(obj)
        % evaluation functions for each set representation
        evaluateNumeric(obj, input)
        evaluateZonotope(obj, Z, evParams)
        evaluatePolyZonotope(obj, c, G, Grest, expMat, id, id_, ind, ind_, evParams)
        evaluateTaylm(obj, input, evParams)
        evaluateConZonotope(obj, c, G, C, d, l, u, options, evParams)
        % sensitivity analysis
        evaluateSensitivity(obj, S, x)
    end
end