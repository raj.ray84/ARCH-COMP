function han = plot(I,varargin)
% plot - plots a projection of an interval 
%
% Syntax:
%    han = plot(I)
%    han = plot(I,dims)
%    han = plot(I,dims,type)
%
% Inputs:
%    I - interval object
%    dims - (optional) dimensions for projection
%    type - (optional) plot settings (LineSpec and Name-Value pairs)
%
% Outputs:
%    han - handle to the graphics object
%
% Example: 
%    I = interval([1; -1], [2; 1]);
%    plot(I)
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:       Matthias Althoff
% Written:      31-July-2016
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

%convert to zonotope
Z = zonotope(I);

%plot zonotope
if nargin == 1
    han = plot(Z);
else
    han = plot(Z,varargin{:});
end

if nargout == 0
    clear han;
end

%------------- END OF CODE --------------