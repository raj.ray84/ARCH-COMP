function Rint = pancakeApproach(R,guard,sys,options)
% pancakeApproach - Calculate the instersection with the guard sets by
%                   time scaling of the dynamic function
%
% Syntax:  
%    Rint = pancakeApproach(R,guard,sys,options)
%
% Inputs:
%    R - initial set
%    guard - guard set (halfspace or constrainedHyperplane)
%    sys - dynamical system
%    options - struct with settings for reachability analysis
%
% Outputs:
%    Rint - set representing the intersection with the guard set
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none
%
% References: 
%   [1] S. Bak et al. "Time-Triggered Conversion of Guards for Reachability
%       Analysis of Hybrid Automata"

% Author:       Niklas Kochdumper
% Written:      05-November-2018
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------


%--------------------------------------------------------------------------
%                         CREATE SCALED SYSTEM
%--------------------------------------------------------------------------

% get direction vector of hyperplane
if isa(guard,'constrainedHyperplane')
    hp = guard.h;
else
    hp = guard;
end

% define scaling function
g = @(x) hp.c' * x;

% get system dynamics
if isa(sys,'linearSys')
   f = @(x,u) dynamicsLinSys(x,u,sys); 
   n = length(sys.A);
   m = size(sys.B,2);
else
   f = sys.mFile; 
end

% time scaled system dynamics
F = @(x,u,t) g(x) * f(x,u);

% create symbolic variables
xSym = sym('x',[n,1]);
uSym = sym('u',[m,1]);
tSym = sym('t');

% create file path
name = [sys.name '_timeScaled'];
path = [coraroot filesep 'models' filesep 'Cora' filesep name];

% create file for time scaled dynamics
func = F(xSym,uSym,tSym);
matlabFunction(func,'File',path,'Vars',{tSym,xSym,uSym});

% create time scaled system
str = ['sysScal = nonlinearSys(n,m,[@' name '],options);'];
eval(str);




%--------------------------------------------------------------------------
%                          TIME SCALING MODE
%--------------------------------------------------------------------------

% define threshold distance when scaling is stopped
dThres = 1e-5;

% initialize variables
Rtemp = R;
Rfin_ = R;

% adapte reachability options
options.tStart = 0;
options.tFinal = options.timeStep;
options.reachabilitySteps = 1;
options.R0 = R;

% scale until hyperplane is intersected or minimum distance is reached
while ~isIntersecting(hp,Rtemp)
   
   Rfin = Rfin_;
   options.R0 = Rfin;
    
   % compute reachable set
   [Rcont,Rtp] = reach(sysScal,options);
   Rtemp = Rcont{end}{1};
   Rfin_ = Rtp{end}{1}.set;
   
   % check if minimum distance is reached
   d = distPlaneLower(hp,Rfin_);
   if abs(d) < dThres
      break; 
   end
end


%--------------------------------------------------------------------------
%                               JUMP
%--------------------------------------------------------------------------

% compute reachable set
[Rcont,Rtp] = reach(sys,options);

% check if the hyperplane has been crossed
d = distPlaneUpper(hp,Rtp{end});

% initialize search domain for optimal time step size
ub = options.timeStep;
lb = 0;

% increase time step until the hyperplane has been crossed
while d < 0 
    
    options.timeStep = 2 * options.timeStep;
    options.tFinal = options.timeStep;
    
    % compute reachable set
    [Rcont,Rtp] = reach(sys,options);
    
    % compute distance from hyperplane
    d_ = distPlaneUpper(hp,Rtp{end});
    
    % check for convergence
    if d_ > d
       error('Pancake approach failed!'); 
    else
       d = d_;
    end
    
    % update search domain for optimal time step size
    ub = options.timeStep;
    lb = options.timeStep/2;
end

% iteratively decrease time step size to find optimal time step
while d < 0 || abs(d) > dThres
   
    options.timeStep = (ub + lb)/2;
    options.tFinal = options.timeStep;
    
    % compute reachable set
    [Rcont,Rtp] = reach(sys,options);
    
    % compute distance from hyperplane
    d = distPlaneUpper(hp,Rtp{end});
    
    % check for convergence
    if d > 0
       ub = options.timeStep;
    else
       lb = options.timeStep;
    end
end

% compute intersection with the hyperplane
cZ = conZonotope(Rcont{end});
Rint = cZ & guard;



% Auxiliary Functions -----------------------------------------------------

function f = dynamicsLinSys(x,u,sys)

    if isempty(sys.c)
       f = sys.A * x + sys.B * u; 
    else
       f = sys.A * x + sys.B * u + sys.c;
    end
    
function d = distPlaneUpper(hp,R)

    if isa(R,'conZonotope')      
        d = hp.d - boundDir(R,hp.c,'upper');        
    else       
        R = hp.c' * R;
        d = hp.d - (R.Z(1) + sum(abs(R.Z(2:end))));      
    end
    
function d = distPlaneLower(hp,R)

    if isa(R,'conZonotope')      
        d = hp.d - boundDir(R,hp.c,'lower');        
    else       
        R = hp.c' * R;
        d = hp.d - (R.Z(1) - sum(abs(R.Z(2:end))));      
    end    
    
%------------- END OF CODE --------------
        