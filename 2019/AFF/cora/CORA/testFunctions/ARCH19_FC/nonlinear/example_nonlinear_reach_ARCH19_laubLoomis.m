function example_nonlinear_reach_ARCH19_laubLoomis()
% example_nonlinear_reach_ARCH19_laubLoomis - example of 
% nonlinear reachability analysis
%
% Syntax:  
%    example_nonlinear_reach_ARCH19_laubLoomis
%
% Inputs:
%    no
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% 
% Author:       Matthias Althoff, Niklas Kochdumper
% Written:      27-March-2019
% Last update:  ---
% Last revision:---


%------------- BEGIN CODE --------------


dim=7;

% Options -----------------------------------------------------------------

options.tStart=0; %start time
options.tFinal=20; %final time
options.x0=[1.2; 1.05; 1.5; 2.4; 1; 0.1; 0.45]; %initial state for simulation
options.R0=quadZonotope(options.x0,0.1*eye(dim),[],[],[]); %initial state for reachability analysis

options.timeStep=0.01; %time step size for reachable set computation
options.taylorTerms=4; %number of taylor terms for reachable sets
options.zonotopeOrder=50; %zonotope order
options.intermediateOrder=5;
options.reductionTechnique='girard';
options.errorOrder=2;
options.polytopeOrder=10; %polytope order
options.reductionInterval=1e5;
options.maxError = 150*ones(dim,1);
options.verbose = 0;

options.plotType='frame';
options.projectedDimensions=[1 2];

options.originContained = 0;
options.advancedLinErrorComp = 1;
options.tensorOrder = 3;


%obtain uncertain inputs
options.uTrans = 0;
options.U = zonotope([0]); %input for reachability analysis




% REACHABILITY ANALYSIS ---------------------------------------------------

R = cell(3,1);
timeStep = cell(3,1);

% Initial set W = 0.1
W = 0.1;
options.R0 = quadZonotope(options.x0,W*eye(dim),[],[],[]);
timeStep{1} = options.timeStep;
fun1 = @(t,x,u) sevenDimNonlinEq(t,x,u);
sys=nonlinearSys(7,1,fun1,options); 
tic
R{1}= reach(sys, options);
tComp = toc;
width = 2*rad(interval(project(R{1}{end}{1},4)));
disp(['computation time of reachable set (W=0.1): ',num2str(tComp)]);
disp(['width of final reachable set (W=0.1): ',num2str(width)]);
disp(' ');


% Initial set W = 0.05
W = 0.05;
options.R0 = zonotope([options.x0,W*eye(dim)]);
options.timeStep = 0.025;
options.errorOrder = 1;
timeStep{2} = options.timeStep;
fun2 = @(t,x,u) sevenDimNonlinEq(t,x,u);
sys=nonlinearSys(7,1,fun2,options); 
tic
R{2} = reach(sys, options);
tComp = toc;
width = 2*rad(interval(project(R{2}{end}{1},4)));
disp(['computation time of reachable set (W=0.05): ',num2str(tComp)]);
disp(['width of final reachable set (W=0.05): ',num2str(width)]);
disp(' ');


% Initial set W = 0.01
W = 0.01;
options.R0 = zonotope([options.x0,W*eye(dim)]);
options.timeStep = 0.1;
options.errorOrder = 1;
options.tensorOrder = 2;
options.advancedLinErrorComp = 0;
timeStep{3} = options.timeStep;
fun3 = @(t,x,u) fun1(t,x,u);
sys=nonlinearSys(7,1,fun3,options); 
tic
R{3} = reach(sys, options);
tComp = toc;
width = 2*rad(interval(project(R{3}{end}{1},4)));
disp(['computation time of reachable set (W=0.01): ',num2str(tComp)]);
disp(['width of final reachable set (W=0.01): ',num2str(width)]);
disp(' ');


% SIMULATION --------------------------------------------------------------

runs = 60;
fractionVertices = 0.5;
fractionInputVertices = 0.5;
inputChanges = 6;
simRes = simulate_random(sys, options, runs, fractionVertices, fractionInputVertices, inputChanges);




% VISUALIZATION -----------------------------------------------------------

colors = {'b',[0,0.6,0],'r'};

figure
hold on

%plot results over time
for k = 1:length(R)
    
    t1 = 0;
    t2 = timeStep{k};
    init = 1;

    %plot time elapse
    for i=1:length(R{k})
        for j=1:length(R{k}{i}) 
            %get Uout 
            Uout1 = interval(project(R{k}{i}{j},4));
            %obtain times
            if init
                init = 0;
            else
                t1 = t1 + timeStep{k};
                t2 = t2 + timeStep{k};
            end
            %generate plot areas as interval hulls
            IH1 = interval([t1; infimum(Uout1)], [t2; supremum(Uout1)]);

            plotFilled(IH1,[1 2],colors{k},'EdgeColor','none');
        end
    end

%     %plot simulation results
%     for i=1:(length(simRes.t))
%         plot(simRes.t{i},simRes.x{i}(:,4),'Color',0*[1 1 1]);
%     end
end

box on
xlabel('t')
ylabel('x_4')
axis([0,20,1.5,5])


%------------- END OF CODE --------------