Hylaa repeatability package for 2019 ARCH-Comp Linear System Group, prepared by Stanley Bak.

Instructions:

Build the container
> docker build . -t hylaa


Run unit tests (sanity check, may have warnings but all tests should pass):
> docker run hylaa


To run an individual model / spec from the competition, first go to the corresponding folder and see the README on the command to run. Run the command you want using a bash terminal within the docker container:

> docker run -it hylaa bash
> cd /re/building
> python3 BLDC01.py safe



To run all the models with one command, saving stdout to a file while echoing it to the terminal (takes about an hour on my computer):

> time (docker run hylaa bash -x /re/run_all_benchmarks 2>&1) | tee stdout.txt


